# An Awesome Design Dynamic Lyrics
=============

## Overview
This is a ready made Design Dynamic Lyrics for you! I created an awesome design that will match your like to include an anime and sort of various genres of music!

From [iTunes](http://www.apple.com/itunes/download/), I extract the simple cover and compile all of my favorite music and made an automatic lyric. This platform enables to follow the song lyrics in an instant. With the special features, I summed-up the well-known anime songs just for you! Enjoy the lyrics and the song! You can visit my [website](http://paydayloansnow.co.uk) for more information!


## Features

#### 1、Menubar Lyrics

![Imgur](http://i.imgur.com/QupHK.png)

#### 2、Desktop Lyrics

![Imgur](http://i.imgur.com/QwRQG.jpg)

#### 3、Album Artwork Filler

![Imgur](http://i.imgur.com/gv9FK.png)

## More Details
* Xcode Version 4.3.2 (4E2002)
* Song lyrics will be automatically downloaded, no need to add manually
* Automatically translate Simplified Chinese into Traditional Chinese

## TODO

1. [DONE] ~~Add CheckUpdate system~~
2. [DONE] ~~Maybe a better icon?~~
3. [ABANDON] ~~Automatical translator~~
4. [DONE] ~~Export album artwork to desktop~~
5. [ABANDON] ~~Automatically write lyrics to iTunes(this may cost a lot of time before playing)~~
6. [DONE] ~~Move some settings to menu, delete them from pref window.~~
7. [DONE] ~~Sandbox App~~
8. [NEW] 更多的语言支持

#
# License

Code in the DynamicLyrics project is licensed under the [GNU General Public License](http://www.gnu.org/licenses/gpl.html).